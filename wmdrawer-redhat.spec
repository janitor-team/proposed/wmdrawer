%define name wmdrawer
%define version 0.10.5
%define release 1

Summary: Retractable button bar launcher dockapp
Name: %{name}
Version: %{version}
Release: %{release}
Source: http://people.easter-eggs.org/~valos/wmdrawer/%{name}-%{version}.tar.gz
License: GPL
Group: X11/WindowMaker Applets
URL: http://people.easter-eggs.org/~valos/wmdrawer/
#Distribution: 
BuildRoot: %{_tmppath}/%{name}-buildroot
Prefix: %{_prefix}
%description
wmDrawer is a dock application (dockapp) which provides a drawer
(retractable button bar) to launch applications.

%prep
rm -rf $RPM_BUILD_ROOT

%setup

%build
make

%install
mkdir -p $RPM_BUILD_ROOT%{prefix}/bin
mkdir -p $RPM_BUILD_ROOT%{prefix}/man/man1
install -m 755 wmdrawer $RPM_BUILD_ROOT%{prefix}/bin
install -m 644 doc/wmdrawer.1x.gz $RPM_BUILD_ROOT%{prefix}/man/man1

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,0755)
%doc wmdrawerrc.example AUTHORS ChangeLog COPYING INSTALL README TODO
%{prefix}/bin/*
%{prefix}/man/man1/*

%changelog
* Mon Nov 11 2002 Kevin Burtch <kburtch@worldnet.att.net>
- Created spec file (for RedHat 8.0, should work on others).

* Fri Feb 7 2003 Val�ry Febvre <vfebvre@vfebvre.lautre.net>
- Rename file CHANGES to Changelog
